﻿using System;
using System.Collections.Generic;
using System.Text;
using ArithmeticExpression.Tokens;

namespace ArithmeticExpression
{
    public class ArithmeticParser
    {
        private readonly string input;
        private readonly HashSet<string> knownBinaryOperations = new HashSet<string> {"+", "-", "*", "/", "^"};
        private readonly HashSet<string> knownConstants = new HashSet<string> {"e", "pi"};
        private readonly HashSet<string> knownFunctions = new HashSet<string> {"sin", "cos", "sqrt"};
        private readonly HashSet<string> knownUnaryOperations = new HashSet<string> {"+", "-"};
        private int parenthesesBalance;
        private int pos;

        public ArithmeticParser(string input)
        {
            this.input = input;
            pos = 0;
            parenthesesBalance = 0;
        }

        private char CurrentChar => input[pos];

        private Token LastToken { get; set; }

        public IEnumerable<Token> Parse()
        {
            while (!IsLineEnded())
                yield return LastToken = ReadToken();
        }

        private Token ReadToken()
        {
            SkipWhitespaces();
            if (char.IsDigit(CurrentChar))
                return ReadNumber();
            if (CurrentChar == '(' || CurrentChar == ')')
                return ReadParenthesis();

            if (IsBinaryOperation())
            {
                var operation = new BinaryOperator(CurrentChar.ToString());
                MovePos();
                return operation;
            }

            if (IsUnaryOperation())
            {
                var operation = new UnaryOperator(CurrentChar.ToString());
                MovePos();
                return operation;
            }

            return ReadLiteral();
        }

        private void SkipWhitespaces()
        {
            while (char.IsWhiteSpace(CurrentChar))
                MovePos();
        }

        private Parenthesis ReadParenthesis()
        {
            var result = CurrentChar == '('
                ? (Parenthesis) new OpeningParenthesis(CurrentChar.ToString())
                : new ClosingParenthesis(CurrentChar.ToString());

            parenthesesBalance += result is OpeningParenthesis ? 1 : -1;
            CheckParenthesesBalance();

            MovePos();
            return result;
        }

        private void CheckParenthesesBalance()
        {
            if (parenthesesBalance < 0)
                throw new Exception($"Unexpected parentheses at position {pos}\n{input}\n{new string('-', pos) + "^"}");
        }

        private bool IsLineEnded()
        {
            return pos >= input.Length;
        }

        private Number ReadNumber()
        {
            var number = new StringBuilder();
            while (!IsLineEnded() && char.IsDigit(CurrentChar))
            {
                number.Append(CurrentChar);
                MovePos();
            }

            if (IsLineEnded() || CurrentChar != '.')
                return new Integer(number.ToString());

            number.Append(CurrentChar);
            MovePos();

            while (!IsLineEnded() && char.IsDigit(CurrentChar))
            {
                number.Append(CurrentChar);
                MovePos();
            }

            if (!IsLineEnded() && CurrentChar == '.')
                throw new Exception($"Unexpected token {CurrentChar} at position {pos}");

            return new Float(number.ToString());
        }


        private bool IsBinaryOperation()
        {
            return knownBinaryOperations.Contains(CurrentChar.ToString()) &&
                   (LastToken is Number || LastToken is Constant || LastToken is ClosingParenthesis);
        }

        private bool IsUnaryOperation()
        {
            return knownUnaryOperations.Contains(CurrentChar.ToString()) &&
                   (pos == 0 || LastToken is OpeningParenthesis || LastToken is UnaryOperator);
        }

        private Literal ReadLiteral()
        {
            var literalBuilder = new StringBuilder();
            while (!IsLineEnded() && char.IsLetter(CurrentChar))
            {
                literalBuilder.Append(CurrentChar);
                MovePos();
            }

            var literal = literalBuilder.ToString();
            if (knownFunctions.Contains(literal))
                return new Function(literal);
            if (knownConstants.Contains(literal))
                return new Constant(literal);
            throw new Exception($"Unknown token {literal} at position {pos - literal.Length}");
        }

        private void MovePos()
        {
            ++pos;
        }
    }
}