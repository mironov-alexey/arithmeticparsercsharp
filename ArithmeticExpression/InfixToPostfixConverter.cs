﻿using System;
using System.Collections.Generic;
using System.Linq;
using ArithmeticExpression.Tokens;

namespace ArithmeticExpression
{
    public class InfixToPostfixConverter
    {
        private readonly IEnumerable<Token> tokens;

        public InfixToPostfixConverter(IEnumerable<Token> tokens)
        {
            this.tokens = tokens;
        }

        public IEnumerable<Token> Convert()
        {
            var stack = new Stack<Token>();
            foreach (var token in tokens)
                switch (token)
                {
                    case Number _:
                    case Constant _:
                        yield return token;
                        break;
                    case Function _:
                        stack.Push(token);
                        break;
                    case Operator operator1:
                    {
                        while (stack.Any() && stack.Peek() is Operator operator2 &&
                               (operator1.Associativity == Associativity.Left && GetOperatorPriority(operator1) <= GetOperatorPriority(operator2) ||
                                operator1.Associativity == Associativity.Right && GetOperatorPriority(operator1) < GetOperatorPriority(operator2)))
                            yield return stack.Pop();
                        stack.Push(operator1);
                        break;
                    }
                    case OpeningParenthesis _:
                        stack.Push(token);
                        break;
                    case ClosingParenthesis _:
                    {
                        var parenthesisMatched = false;
                        while (stack.Any() && !(stack.Peek() is OpeningParenthesis)) yield return stack.Pop();
                        stack.Pop();
                        if (stack.Any())
                            if (stack.Peek() is Function)
                                yield return stack.Pop();
                        break;
                    }
                    default:
                        throw new Exception($"Unknown token {token}");
                }

            while (stack.Any()) yield return stack.Pop();
        }

        private static int GetOperatorPriority(Operator @operator)
        {
            switch (@operator)
            {
                case BinaryOperator op when op.Value == "+" || op.Value == "-":
                    return 1;
                case BinaryOperator op when op.Value == "*" || op.Value == "/":
                    return 2;
                case BinaryOperator op when op.Value == "^":
                    return 3;
                case UnaryOperator _:
                    return int.MaxValue;
                default:
                    return int.MinValue;
            }
        }
    }
}