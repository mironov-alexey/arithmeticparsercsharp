﻿using System;
using System.Linq;
using ArithmeticExpression.Tokens;

namespace ArithmeticExpression
{
    internal class Program
    {
        public static void Main(string[] args)
        {
//            var input = "-1 +   2 * (3 + 4) ^ 5 / sin(pi * 3) - e";
//            const string input = "sin(1 + 2) * 3";
//            const string input = "sin(-(2 ^ 2) * pi^(2.2^2)^3) ^ 3";
//            const string input = "sin(-(2 ^ 2) * pi^(2.2^2)^3) ^ 3";
//            const string input = "sin(-(2 ^ 2) * pi^(2.2^2)^3)";
//            const string input = "sqrt(++2)^2";
            var input = Console.ReadLine();
            var parser = new ArithmeticParser(input);
            var tokens = parser.Parse().ToArray();
            Console.WriteLine($"Raw input:            {input}");
            Console.WriteLine($"Tokenized input:      {string.Join(" ", tokens.Select(t => t.ToString()))}");
            var infixToPostfixConverter = new InfixToPostfixConverter(tokens);
            var tokensInPostfix = infixToPostfixConverter.Convert().ToArray();
            Console.WriteLine($"Converted to postfix: {string.Join(" ", tokensInPostfix.Select(t => t.ToString()))}");
            var evaluator = new Evaluator(tokensInPostfix);
            Console.WriteLine($"Result of evaluation: {evaluator.Evaluate()}");
        }
    }
}