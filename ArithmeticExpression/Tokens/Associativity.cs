﻿namespace ArithmeticExpression.Tokens
{
    public enum Associativity
    {
        Left,
        Right
    }
}