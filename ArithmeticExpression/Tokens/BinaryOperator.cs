﻿using System.Collections.Generic;

namespace ArithmeticExpression.Tokens
{
    public class BinaryOperator : Operator
    {
        private static readonly Dictionary<string, Associativity> OperatorsAssociativity =
            new Dictionary<string, Associativity>
            {
                ["+"] = Associativity.Left,
                ["-"] = Associativity.Left,
                ["*"] = Associativity.Left,
                ["/"] = Associativity.Left,
                ["^"] = Associativity.Right
            };

        public BinaryOperator(string value) : base(value)
        {
        }

        public override Associativity Associativity => OperatorsAssociativity[Value];
    }
}