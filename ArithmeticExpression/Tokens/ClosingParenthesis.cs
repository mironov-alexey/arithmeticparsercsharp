﻿namespace ArithmeticExpression.Tokens
{
    public class ClosingParenthesis : Parenthesis
    {
        public ClosingParenthesis(string value) : base(value)
        {
        }
    }
}