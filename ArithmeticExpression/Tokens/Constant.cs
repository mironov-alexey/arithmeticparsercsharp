﻿namespace ArithmeticExpression.Tokens
{
    public class Constant : Literal
    {
        public Constant(string value) : base(value)
        {
        }
    }
}