﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ArithmeticExpression.Tokens
{
    public class Evaluator
    {
        private static readonly Dictionary<string, double> constantsValue = new Dictionary<string, double>
        {
            ["e"] = Math.E,
            ["pi"] = Math.PI
        };

        private readonly IEnumerable<Token> tokens;

        public Evaluator(IEnumerable<Token> tokens)
        {
            this.tokens = tokens;
        }

        public double Evaluate()
        {
            var stack = new Stack<double>();
            foreach (var token in tokens)
                switch (token)
                {
                    case Number number:
                        stack.Push(EvaluateNumber(number));
                        break;
                    case Constant constant:
                        stack.Push(EvaluateConstant(constant));
                        break;
                    case UnaryOperator @operator:
                        stack.Push(EvaluateOperation(@operator, stack.Pop()));
                        break;
                    case BinaryOperator @operator:
                        stack.Push(EvaluateOperation(@operator, stack.Pop(), stack.Pop()));
                        break;
                    case Function function:
                        stack.Push(EvaluateFunction(function, stack.Pop()));
                        break;
                    default:
                        throw new Exception($"Unknown token {token}");
                }

            return stack.Pop();
        }

        private double EvaluateFunction(Function function, double argument)
        {
            switch (function.Value)
            {
                case "sin": return Math.Sin(argument);
                case "cos": return Math.Cos(argument);
                case "sqrt": return Math.Sqrt(argument);
                default: throw new Exception($"Unknown function {function.Value}");
            }
        }

        private static double EvaluateNumber(Number number)
        {
            return double.Parse(number.Value, CultureInfo.InvariantCulture);
        }

        private static double EvaluateConstant(Constant constant)
        {
            return constantsValue[constant.Value];
        }

        private static double EvaluateOperation(UnaryOperator @operator, double operand)
        {
            switch (@operator.Value)
            {
                case "+": return operand;
                case "-": return -operand;
                default: throw new Exception($"Unknown operator {@operator.Value}");
            }
        }

        private double EvaluateOperation(BinaryOperator @operator, double operand2, double operand1)
        {
            switch (@operator.Value)
            {
                case "+": return operand1 + operand2;
                case "-": return operand1 - operand2;
                case "*": return operand1 * operand2;
                case "/": return operand1 / operand2;
                case "^": return Math.Pow(operand1, operand2);
                default: throw new Exception($"Unknoww operator {@operator.Value}");
            }
        }
    }
}