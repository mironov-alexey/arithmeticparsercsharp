﻿namespace ArithmeticExpression.Tokens
{
    public abstract class Literal : Token
    {
        protected Literal(string value) : base(value)
        {
        }
    }
}