﻿namespace ArithmeticExpression.Tokens
{
    public class Number : Token
    {
        public Number(string value) : base(value)
        {
        }
    }
}