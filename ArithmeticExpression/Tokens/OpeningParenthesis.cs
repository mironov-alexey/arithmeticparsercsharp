﻿namespace ArithmeticExpression.Tokens
{
    public class OpeningParenthesis : Parenthesis
    {
        public OpeningParenthesis(string value) : base(value)
        {
        }
    }
}