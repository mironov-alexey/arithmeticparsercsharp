﻿namespace ArithmeticExpression.Tokens
{
    public abstract class Operator : Token
    {
        protected Operator(string value) : base(value)
        {
        }

        public virtual Associativity Associativity { get; }
    }
}