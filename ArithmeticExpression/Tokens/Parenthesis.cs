﻿namespace ArithmeticExpression.Tokens
{
    public abstract class Parenthesis : Token
    {
        protected Parenthesis(string value) : base(value)
        {
        }
    }
}