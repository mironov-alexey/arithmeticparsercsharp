﻿namespace ArithmeticExpression.Tokens
{
    public class Token
    {
        public Token(string value)
        {
            Value = value;
        }

        public string Value { get; }

        public override string ToString()
        {
            return Value;
        }
    }
}