﻿namespace ArithmeticExpression.Tokens
{
    public class UnaryOperator : Operator
    {
        public UnaryOperator(string value) : base(value)
        {
        }

        public override Associativity Associativity => Associativity.Right;
    }
}